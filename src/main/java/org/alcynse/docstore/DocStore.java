package org.alcynse.docstore;

import java.util.List;
import org.alcynse.id.IdGenerator;
import org.alcynse.synapse.ExtensionPoint;

/**
 * <p>This is the main (Facade Pattern) access object of Document Store.</p>
 * <p>The document store is designed to a pluggable document storage, where
 * documents are versioned. The item of this storage is the {@link Document},
 * which contains one or more namespaced {@link Property}.</p>
 * 
 * @author Laszlo Solova
 */
public interface DocStore extends ExtensionPoint {

  public void addPropertyTypeHandler(String propertyTypeName,
      Class<PropertyTypeHandler> typeHandlerClazz);
  public void addSavingHandler(SavingHandler handler);
  public void addStoreManager(StoreManager manager);

  /**
   * Creates a typed document. If type is null, then a default document will be
   * created with systemwide required properties only.
   * 
   * @param type The document type or null.
   * @return The new document.
   */
  public Document create(String type);

  /**
   * Creates a typed property instance.
   * 
   * @param type The type of property instance.
   * @return The new property.
   */
  public Property createProperty(String type);

  /**
   * Gives back a markered document with the id.
   * 
   * @param id The document id.
   * @param marker The marker requested or null for default.
   * @return The document instance or null if not found.
   */
  public Document get(String id, String marker);

  /**
   * Gives back the id generator used to create a document type. If no
   * special generator was registered, then the default generator will be back.
   * 
   * @param type The document type or null.
   * @return An IdGenerator instance.
   */
  public IdGenerator getIdGenerator(String type);

  /**
   * Gives back the named store manager.
   * 
   * @param name The name of store manager.
   * @return The casted store manager.
   */
  public <T extends StoreManager> T getStoreManager(String name);
  public void removePropertyTypeHandler(String propertyTypeName);
  public void removeSavingHandler(SavingHandler handler);
  public void removeStoreManager(StoreManager manager);

  /**
   * Persist a document.
   * 
   * @param doc The document to persisting.
   * @return The persisted (updated document).
   * @throws ImmutableDocumentException If the document cannot be changed.
   */
  public Document save(Document doc) throws ImmutableDocumentException;

  /**
   * Search for document ids.
   * 
   * @param searchFilter The filter for search.
   * @return The list of document ids or an empty list.
   * @throws DocStoreException If something went wrong. For more check the
   *           exception message.
   */
  public List<String> search(SearchFilter searchFilter)
      throws DocStoreException;

  /**
   * Search for resolved documents.
   * 
   * @param searchFilter The filter for search.
   * @return The list of documents or an empty list.
   * @throws DocStoreException If something went wrong. For more check the
   *           exception message.
   */
  public List<Document> searchDocs(SearchFilter searchFilter)
      throws DocStoreException;

}
