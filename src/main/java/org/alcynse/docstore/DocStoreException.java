package org.alcynse.docstore;

import org.alcynse.exception.AlcynseException;

/**
 * <p>Default DocStore exception object.</p>
 * 
 * @author Laszlo Solova
 */
public class DocStoreException extends AlcynseException {
  private static final long serialVersionUID = 8831970770868528713L;

  public DocStoreException() {}

  public DocStoreException(String arg0, Throwable arg1, boolean arg2,
      boolean arg3) {
    super(arg0, arg1, arg2, arg3);
  }

  public DocStoreException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  public DocStoreException(String arg0) {
    super(arg0);
  }

  public DocStoreException(Throwable arg0) {
    super(arg0);
  }

}
