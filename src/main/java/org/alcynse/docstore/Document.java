package org.alcynse.docstore;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Set;

/**
 * <p>This is representation of a document, which designed to group properties.
 * This holds a version of the property set and the optional binary. All
 * documents are marked, this marker is a named version of the document.</p>
 * <p><strong>Getting document properties</strong> is a simple get, and there
 * are some simplifying: if no namespace was set, it is resolved as a query
 * interpreted on 'content' namespace.</p>
 * <h2>Binaries</h2>
 * <p>A document can be binary or a simple document with properties only. Binary
 * documents contains a binary stream, which can be handled as simple stream.
 * They have an inner representation of the binary, so the update of binary
 * content updates the meta informations stored as properties.</p>
 * <p>The behaviour of binary documents is similar to normal documents, and the
 * binary contents are stored in separated files. The versioning is very simple,
 * no diffs, no special comparisons - all binary versions are stored as
 * separated file items. This requires higher storage capacity, but the usage
 * very easy.</p>
 * <p>To create a binary document requires no special methods or settings: the
 * difference is only an existing binary stream (a file in the background), so
 * if a binary was set, the document is a binary document, if not, then not.</p>
 * <p>A document can be immutable. Such documents cannot be saved because they
 * cannot be changed. Property overwriting or adding is not allowed, we can get
 * {@link ImmutableDocumentException}.</p>
 * 
 * @author Laszlo Solova
 */
public interface Document {

  /**
   * This is the main (and default) namespace of a document. All properties not
   * namespaced are placed in this space.
   */
  public static final String NAMESPACE_MAIN   = "content";
  /**
   * This is the name of the meta information namespace. This namespace is
   * designed to store all information required by the store or the document
   * itself.
   */
  public static final String NAMESPACE_META   = "docstore";

  /**
   * The separator bytes in document stream.
   */
  public final byte[] BINARY_SEPARATOR = "|_BIN_|".getBytes();

  /**
   * Add a new property to this document. Overwrite existing property if exists.
   * 
   * @param property The new property.
   */
  public void add(Property property) throws ImmutableDocumentException;

  // /**
  // * Clean up loaded properties and reload them again next request.
  // *
  // * @param namespace The property namespace to clean. If null, then all
  // cached
  // * properties will be removed.
  // */
  // public void clear(String namespace);

  /**
   * It gives back the value of a property.
   * 
   * @param propname The name of the property. It can built up after the next
   *          schema: [namespace:]name[/count]
   * @param defaultvalue The default value for return. It can be null.
   * @return The value or null if not exists.
   * @throws ClassCastException If the value cannot be casted to the requested
   *           class.
   */
  public <T> T get(String propname, T defaultvalue) throws ClassCastException;

  /**
   * It gives back all values of a property.
   * 
   * @param propname The name of the property. It can built up after the next
   *          schema: [namespace:]name[/count]
   * @param defaultvalue The default value for return. It can be null. If not
   *          null, then the return list contains this value on the place 0.
   * @return The list of values or null if no such value exists and defaultvalue
   *         is null.
   * @throws ClassCastException If a value cannot be casted to the requested
   *           class.
   */
  public <T> List<T> getAll(String propname, T defaultvalue)
      throws ClassCastException;

  /**
   * If a document contains a binary, then this method gives back an
   * {@link InputStream} to the binary content. If not, then it comes back with
   * null.
   * 
   * @return The binary's InputStream or null.
   */
  public InputStream getBinaryInputStream();

  /**
   * Binary can be written via this {@link OutputStream}. No similarity check is
   * performed, so all write creates new binary file. Be careful, because it
   * requires more resources.
   * The binary can be removed with writing a zero-length byte array.
   * 
   * @return The OutputStream to write binary.
   */
  public OutputStream getBinaryOutputStream();

  /**
   * It gives back the unique ID of this document. A document is unique if this
   * ID combined with the marker differs from another documents.
   * 
   * @return
   */
  public String getDocId();

  /**
   * This input stream is a representation of the whole versioned document. If
   * the document is binary document, then the input stream contains a special
   * separator (defined as BINARY_SEPARATOR byte array in this interface).
   * Before this separator are the properties, after it there can be found
   * binary versions separated with the similar separator.
   * 
   * @return The document input stream.
   */
  public InputStream getDocumentInputStream();

  /**
   * Gives back the timestamp of last modification. Be careful - the last
   * modification time depends on marker of the document.
   * 
   * @return The timestamp when this document was modified.
   */
  public Long getLastModified();

  /**
   * It gives back all markers, which is valid for this document. For example,
   * this document can be the latest and the enabled too, if there is not after
   * enabling edited version.
   * 
   * @return The markers of this document version.
   */
  public Set<String> getMarkers();

  /**
   * All document has a name. The document description can set the field from
   * where this value is read.
   * 
   * @return The name of this document. This can be displayed on the user
   *         interfaces.
   */
  public String getName();

  /**
   * Properties are grouped in namespaces. There are predefined namespaces
   * (docstore, content, binary) and custom namespaces. The custom namespaces
   * can be used to store special information processed by unique handlers or
   * other processors.
   * 
   * @return All namespaces existing in the document version represented by this
   *         object.
   */
  public Set<String> getNamespaces();

  public List<Property> getProperties(String name);

  public Property getProperty(String name);

  public int getPropertyCount(String name);

  public Set<String> getPropertyNames(String namespace);

  /**
   * If this document is schema-based, then this method gives back the name of
   * the schema.
   * 
   * @return The name of document type or null if not exists.
   */
  public String getType();

  /**
   * Tells the document is immutable or not.
   * 
   * @return True, if document cannot be changed.
   */
  public boolean isImmutable();

  /**
   * Persist the document.
   * 
   * @throws DocStoreException If something went wrong.
   * @throws ImmutableDocumentException If the document is immutable and cannot
   *           be saved.
   */
  public void save() throws DocStoreException, ImmutableDocumentException;
}
