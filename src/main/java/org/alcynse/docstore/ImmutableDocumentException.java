package org.alcynse.docstore;

/**
 * <p>This exception will be thrown on saving an immutable document. The
 * Exception has a static instance, you can get it with getInstace() method.</p>
 * 
 * @author Laszlo Solova
 */
public class ImmutableDocumentException extends DocStoreException {

  private static final long                       serialVersionUID = -8775681023713774229L;
  private static final ImmutableDocumentException ide              = new ImmutableDocumentException();

  public static final ImmutableDocumentException getInstance() {
    return ide;
  }

}
