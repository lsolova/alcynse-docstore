package org.alcynse.docstore;

/**
 * <p>The property is an atomic element of the DocStore. A property
 * holds an information item of a document. All properties has a
 * property type, this is an encoding agreement between persistent
 * storage and working copy. This is handled by registered
 * {@link PropertyTypeHandler}s. Please, to see the concept behind
 * check the documentation about storage structures.</p>
 * <p>All property has a full name, which have to be unique with a current
 * marker and a docid. The full name contains the namespace, the real name and
 * the count of this property.</p>
 * 
 * @author Laszlo Solova
 */
public interface Property {

  /**
   * The deleted flag.
   */
  public static final byte FLAG_DELETED = 0x01;

  /**
   * Gives back the count of this property (this is part of unique key).
   * 
   * @return The count number.
   */
  public int getCount();

  /**
   * Gives back the special flags of this property. Flags can be used to mark
   * properties.
   * 
   * @return The binary flags as an integer.
   */
  public int getFlags();

  /**
   * Gives back the full property name: 'namespace:name/count'.
   * 
   * @return The full name of this property (with namespace and count).
   */
  public String getFullName();

  public String getName();

  public String getNamespace();

  /**
   * Gives back the type name of this property. Types are configured in the
   * system.
   * 
   * @return The type name.
   */
  public String getType();

  /**
   * Gives back the actual value of the property. If the value changed, then we
   * can get the changed value, if not, then we can get the original.
   * 
   * @return
   */
  public Object getValue();

  /**
   * The persisted value is changed or not. If yes, then the document is changed
   * too, so it requires saving.
   * 
   * @return True, if changed.
   */
  public boolean isChanged();

  /**
   * Revoke value changes, if it is not persisted. Do nothing if the value is
   * unchanged or it is immutable.
   */
  public void resetValue();

  /**
   * It set up a flag for this property. To revoke a flag, a negated flag can be
   * set.
   */
  public void setFlag(byte flag);

  /**
   * Set/update the value of this property.
   * 
   * @param value The new value to set.
   * @return The property itself.
   * @throws ImmutableDocumentException If the document which this property is
   *           assigned is immutable, so no changes allowed.
   */
  public Property setValue(Object value) throws ImmutableDocumentException;

}
