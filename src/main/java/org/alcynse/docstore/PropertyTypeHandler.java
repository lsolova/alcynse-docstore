package org.alcynse.docstore;

/**
 * <p>The property handler is an encoder/decoder, which is responsible for
 * transform a property type to persistent form and back.</p>
 * 
 * @author Laszlo Solova
 */
public interface PropertyTypeHandler {

  /**
   * Create a new property.
   * 
   * @param name The name of this property. Be careful - the namespace have to
   *          be included if it differs from the default namespace.
   * @return
   */
  public Property create(String name);
  /**
   * Decode property from persistable String object.
   * 
   * @param value The String object.
   * @return The property value in Object. The object type depends on type
   *         handler, for more information consult the documentation, please.
   * @throws DocStoreException If something goes wrong.
   */
  public Object decode(String value) throws DocStoreException;

  /**
   * Encode property to persistable String object.
   * 
   * @param property The original property.
   * @return The persistable String object.
   * @throws DocStoreException If something went wrong.
   */
  public String encode(Property property) throws DocStoreException;

  /**
   * Encode the value to persistable String object.
   * 
   * @param value The original value.
   * @return The persistable String object.
   * @throws DocStoreException If something went wrong.
   */
  public String encode(Object value) throws DocStoreException;
  /**
   * It gives back the name of the handled property type.
   * 
   * @return The name of property type.
   */
  public String getType();

}
