package org.alcynse.docstore;


/**
 * <p>This interface defines the document saving preparation handler, which can
 * check or modify a document before saving, or do some tasks after the document
 * saved.</p>
 * <p>The running order of handlers is random.</p>
 * 
 * @author Laszlo Solova
 */
public interface SavingHandler {
  public enum Type {
    POSTSAVE, PRESAVE,
  }
  public Type getType();
  public void onSave(Document doc) throws DocStoreException;
}
