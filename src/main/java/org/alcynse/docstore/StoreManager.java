package org.alcynse.docstore;

/**
 * <p>This interface defines the document namespace or relation manager, which
 * can handle the document namespace or the relation.</p>
 * <p>The relation is a special document, which describes a relation between two
 * documents. All relation has two node ids and a type included. They can be
 * grouped by the type.</p>
 * 
 * @author Laszlo Solova
 */
public interface StoreManager {
  public enum Type {
    NAMESPACE, RELATION,
  }
  public String getManagedName();
  public Type getType();
}
