package org.alcynse.docstore.internal;

import org.alcynse.docstore.DocStoreException;
import org.alcynse.docstore.Property;
import org.alcynse.docstore.PropertyTypeHandler;

/**
 * <p>This is the abstract property type handler, to manage common methods and
 * variables.</p>
 * 
 * @author Laszlo Solova
 */
public abstract class AbstractPropertyTypeHandler implements
PropertyTypeHandler {

  private String type;

  public AbstractPropertyTypeHandler() {}

  @Override
  public Property create(String name) {
    return new PropertyImpl(type).setName(name);
  }

  @Override
  public String encode(Property property) throws DocStoreException {
    return encode(property.getValue());
  }

  @Override
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

}
