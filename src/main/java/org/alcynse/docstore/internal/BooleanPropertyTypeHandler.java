package org.alcynse.docstore.internal;

import org.alcynse.docstore.DocStoreException;

public class BooleanPropertyTypeHandler extends AbstractPropertyTypeHandler {

  private static final String FALSE_VALUE = "F";
  private static final String TRUE_VALUE  = "T";

  @Override
  public String encode(Object value) throws DocStoreException {
    return (value == null) ? null : ((Boolean) value) ? TRUE_VALUE : FALSE_VALUE;
  }

  @Override
  public Object decode(String value) throws DocStoreException {
    return (value == null || value.trim().isEmpty()) ? null : TRUE_VALUE
        .equals(value);
  }

}
