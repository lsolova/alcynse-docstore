package org.alcynse.docstore.internal;

import org.alcynse.docstore.DocStoreException;

public class IntegerPropertyTypeHandler extends AbstractPropertyTypeHandler {

  @Override
  public String encode(Object value) throws DocStoreException {
    return value != null ? String.valueOf(value) : null;
  }

  @Override
  public Object decode(String value) throws DocStoreException {
    return (value == null || value.trim().isEmpty()) ? null : Integer
        .valueOf(value);
  }

}
