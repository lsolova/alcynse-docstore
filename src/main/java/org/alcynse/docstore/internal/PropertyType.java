package org.alcynse.docstore.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.alcynse.docstore.PropertyTypeHandler;
import org.alcynse.docstore.Validator;

/**
 * <p>It describe a concrete property instance type.</p>
 * 
 * @author Laszlo Solova
 */
public class PropertyType {
  private int                 id;
  private String              name;
  private PropertyTypeHandler handler;
  private List<Validator>     validators;

  private PropertyType() {}

  public static class Builder {
    private int                 id;        // required
    private String              name;      // required
    private PropertyTypeHandler handler;   // required
    private List<Validator>     validators; // optional

    public Builder(Integer id, String name, PropertyTypeHandler handler) {
      this.id = id;
      this.name = name;
      this.handler = handler;
    }

    public Builder addValidator(Validator validator) {
      if (validators == null)
        validators = new ArrayList<Validator>();
      return this;
    }

    public PropertyType build() {
      PropertyType pt = new PropertyType();
      pt.handler = this.handler;
      pt.id = this.id;
      pt.name = this.name;
      pt.validators = (this.validators == null) ? null : Collections
          .unmodifiableList(this.validators);
      return pt;
    }
  }

  /* TODO Remove temporary setters after finishing new configuration service. */
  public PropertyTypeHandler getHandler() {
    return handler;
  }
  public void setHandler(PropertyTypeHandler handler) {
    this.handler = handler;
  }
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public List<Validator> getValidators() {
    return validators;
  }

}
