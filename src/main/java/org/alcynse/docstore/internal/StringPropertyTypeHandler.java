package org.alcynse.docstore.internal;

import org.alcynse.docstore.DocStoreException;

public class StringPropertyTypeHandler extends AbstractPropertyTypeHandler {

  @Override
  public String encode(Object value) throws DocStoreException {
    return value != null ? ((String) value).trim() : null;
  }

  @Override
  public Object decode(String value) throws DocStoreException {
    return value;
  }

}
