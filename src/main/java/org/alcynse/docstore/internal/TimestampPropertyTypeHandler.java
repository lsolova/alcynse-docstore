package org.alcynse.docstore.internal;

import org.alcynse.docstore.DocStoreException;
import org.alcynse.util.DateTimeUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

public class TimestampPropertyTypeHandler extends AbstractPropertyTypeHandler {

  private static final DateTimeFormatter MILLISECOND_FORMATTER = DateTimeUtils.getMillisecondFormatter();

  @Override
  public String encode(Object value) throws DocStoreException {
    DateTime dtValue = (DateTime) value;
    if (dtValue == null)
      return null;
    String persistingValue = MILLISECOND_FORMATTER.print(
        dtValue);
    while (persistingValue.endsWith("0"))
      persistingValue = persistingValue.substring(0,
          persistingValue.length() - 1);
    return persistingValue;
  }

  @Override
  public Object decode(String value) throws DocStoreException {
    if (value == null)
      return null;
    while (value.length() < 15)
      value += "0";
    return MILLISECOND_FORMATTER.parseDateTime(value);
  }

}
