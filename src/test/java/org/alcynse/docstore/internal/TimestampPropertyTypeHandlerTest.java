package org.alcynse.docstore.internal;

import junit.framework.Assert;
import org.alcynse.docstore.Property;
import org.alcynse.util.DateTimeUtils;
import org.joda.time.DateTime;
import org.junit.Test;

/**
 * @author Laszlo
 */
public class TimestampPropertyTypeHandlerTest {
  @Test
  public void createTimestampProperty() throws Exception {

    long currentTimeMillis = System.currentTimeMillis();
    String value = DateTimeUtils.getMillisecondFormatter().print(
        currentTimeMillis);

    TimestampPropertyTypeHandler tph = new TimestampPropertyTypeHandler();
    Property p = tph.create("testTime");
    p.setValue(new DateTime(currentTimeMillis));
    Assert.assertEquals(value, tph.encode(p));
  }
}
